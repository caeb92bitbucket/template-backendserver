/*
    -- Para matar procesos --

    MacBook-Pro-de-Camilo:backend-server camilo$ lsof -t -i :3000
    Respuesta => 3217

    MacBook-Pro-de-Camilo:backend-server camilo$ kill -9 3217
    [1]+  Killed: 9               node app

*/

// Requires - Importacion de librerias
var express = require ('express');
var mongoose = require ('mongoose');

// Inicializar variables
var app = express();

// Conexion a la BDD
mongoose.connection.openUri('mongodb://localhost:27017/photoPrizeDB', ( err, res )=>{
    if(err) throw err;
    console.log('Conexion a PhotoPrize DB: \x1b[32m%s\x1b[0m', 'online');
}); 

// Rutas
app.get('/', (req, res, next) => {
    res.status(200).json({
        ok: true,
        mensaje: 'Peticion correcta'
    })
});

// Escuchar peticiones
app.listen(3000,() => {
    console.log('Express corriendo en el puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});